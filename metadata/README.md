# F-Droid Assets
This folder contains the assets used on [Stay Put's F-Droid store page](https://f-droid.org/packages/org.y20k.stayput/).
More about adding assets -> [All About Descriptions, Graphics, and Screenshots](https://f-droid.org/docs/All_About_Descriptions_Graphics_and_Screenshots/).
