# v0.8.0 - Medicine Cabinet Pirate

**2025-01-12**
- initial release

Important: Stay Put has undergone thorough testing prior to release. However, minor bugs may surface as more users start interacting with the app. Please keep in mind that Stay Put is not intended to be a foolproof theft alert system.