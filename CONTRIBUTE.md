How to contribute to Stay Put
==============================

### Help with translations
The translations are managed on [Weblate](https://hosted.weblate.org/projects/stayput/).

### Submit your own solutions
Stay Put does not accept pull requests. The app is a one-person project. Managing pull requests is too time consuming.