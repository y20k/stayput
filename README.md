README
======

# Stay Put - Unplug Alert

<img src="https://codeberg.org/y20k/stayput/raw/branch/main/metadata/en-US/images/icon.png" width="192" />

**Version 0.8.x ("Medicine Cabinet Pirate")**

Stay Put is an app that helps prevent casual theft of your device while charging in public spaces. The app was created to solve a common problem: charging your phone in public spaces while traveling or camping. When you need to leave your phone unattended at a public charging spot, Stay Put acts as your guard. It sounds a loud alarm if someone unplugs your device, deterring potential theft.

Stay Put is free software. It is published under the [MIT open source license](https://opensource.org/licenses/MIT). Want to help? Please check out the notes in [CONTRIBUTE.md](https://codeberg.org/y20k/stayput/src/branch/main/CONTRIBUTE.md) first.

## Core Features
- Loud alarm when device is unplugged
- Secure disarm with biometric authentication or device credentials
- Option to allow disarm by plugging the device back to power
- No tracking, no data collection

## Install Stay Put
[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" height="80">](https://f-droid.org/packages/org.y20k.stayput/) [<img src="https://gitlab.com/IzzyOnDroid/repo/-/raw/master/assets/IzzyOnDroid.png" alt="Get it on IzzyOnDroid" height="80">](https://apt.izzysoft.de/packages/org.y20k.stayput/)

## Screenshots
[<img src="https://codeberg.org/y20k/stayput/raw/branch/main/metadata/en-US/images/phoneScreenshots/01.png" width="240">](https://codeberg.org/y20k/stayput/raw/branch/main/metadata/en-US/images/phoneScreenshots/01.png) [<img src="https://codeberg.org/y20k/stayput/raw/branch/main/metadata/en-US/images/phoneScreenshots/02.png" width="240">](https://codeberg.org/y20k/stayput/raw/branch/main/metadata/en-US/images/phoneScreenshots/02.png) [<img src="https://codeberg.org/y20k/stayput/raw/branch/main/metadata/en-US/images/phoneScreenshots/03.png" width="240">](https://codeberg.org/y20k/stayput/raw/branch/main/metadata/en-US/images/phoneScreenshots/03.png)

## FAQ
### When should I use Stay Put?
Stay Put is ideal when charging your phone in public spaces like:
- Public libraries
- Hostels
- Campground bathrooms
- Airport charging stations
- Any shared charging area

### How does Stay Put prevent theft?
Stay Put doesn't physically prevent theft, but it acts as a deterrent. The moment someone unplugs your phone, a loud alarm sounds. The alarm can only be disabled using your biometric data or device credentials – or by plugging the device back to power (if this option is enabled).

### Can I still use my phone while Stay Put is active?
Yes, Stay Put runs in the background and doesn't interfere with normal phone usage. Just remember to disarm it before unplugging your phone.
