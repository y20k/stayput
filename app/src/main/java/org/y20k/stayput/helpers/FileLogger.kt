package org.y20k.stayput.helpers

import android.content.Context
import android.util.Log
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

object FileLogger {
    private const val LOG_FILE = "stayput_debug.log"
    private val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault())

    fun log(context: Context, tag: String, message: String) {
        val timestamp = dateFormat.format(Date())
        val logMessage = "$timestamp: $message\n"

        try {
            val file = File(context.getExternalFilesDir(null), LOG_FILE)
            file.appendText(logMessage)
            Log.d(tag, "Logged to file: $message")
        } catch (e: Exception) {
            Log.e(tag, "Error writing to log file", e)
        }
    }

    fun getLogFile(context: Context): File {
        return File(context.getExternalFilesDir(null), LOG_FILE)
    }

}