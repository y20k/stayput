/*
 * NotificationHelper.kt
 * Implements the NotificationHelper class
 * A NotificationHelper creates and configures a notification
 *
 * This file is part of
 * STAY PUT - Unplug Alert App
 *
 * Copyright (c) 2022-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package org.y20k.stayput.helpers

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import org.y20k.stayput.Keys
import org.y20k.stayput.MainActivity
import org.y20k.stayput.R


/*
 * NotificationHelper class
 */
object NotificationHelper {

    /* Define log tag */
    private val TAG: String = NotificationHelper::class.java.simpleName


    /* Creates the and shows the observer notification */
    fun showObserverNotification(context: Context): Notification {
        val notificationManager: NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        // create notification channel if necessary
        if (notificationManager.getNotificationChannel(Keys.CHANNEL_ID_OBSERVING_DEVICE_STATE) == null) {
            notificationManager.createObserverNotificationChannel(context)
        }
        // create pending intent
        val showActionPendingIntent: PendingIntent? = TaskStackBuilder.create(context).run {
            addNextIntentWithParentStack(Intent(context, MainActivity::class.java))
            getPendingIntent(10, PendingIntent.FLAG_IMMUTABLE) }
        // build and show notification
        val builder = NotificationCompat.Builder(context, Keys.CHANNEL_ID_OBSERVING_DEVICE_STATE)
            .setContentIntent(showActionPendingIntent)
            .setSmallIcon(R.drawable.ic_notification_observation_small_icon_24dp)
            .setContentTitle(context.getString(R.string.notification_observer_title))
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .setCategory(NotificationCompat.CATEGORY_SERVICE)
        return builder.build().also {
            notificationManager.notify(Keys.NOTIFICATION_ID_OBSERVING_DEVICE_STATE, it)
        }
    }


    /* Creates and shows the alarm notification */
    fun showAlertNotification(context: Context): Notification {
        val notificationManager: NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        // create notification channel if necessary
        if (notificationManager.getNotificationChannel(Keys.CHANNEL_ID_ALERT_USER) == null) {
            notificationManager.createAlertNotificationChannel(context)
        }
        // build and show a "Heads-up notification" https://developer.android.com/develop/ui/views/notifications#Heads-up
        // important settings for "Heads-up notification" and fullscreen alert on lockscreen: setPriority(NotificationCompat.PRIORITY_HIGH) & .setFullScreenIntent()
        val builder = NotificationCompat.Builder(context, Keys.CHANNEL_ID_ALERT_USER)
            .setSmallIcon(R.drawable.ic_notification_alert_small_icon_24dp)
            .setContentTitle(context.getString(R.string.notification_alert_title))
            .setContentText(context.getString(R.string.notification_alert_content))
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setCategory(NotificationCompat.CATEGORY_ALARM)
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setFullScreenIntent(getFullScreenIntent(context), true)
        return builder.build().also {
            notificationManager.notify(Keys.NOTIFICATION_ID_ALERT_USER, it)
        }
    }


    /* Creates the channel for the observer service notification  */
    private fun NotificationManager.createObserverNotificationChannel(context: Context) {
        val name: String = context.getString(R.string.notification_channel_observing_device_state_name)
        val descriptionText: String = context.getString(R.string.notification_channel_observing_device_state_description)
        val importance: Int = NotificationManager.IMPORTANCE_LOW
        val channel = NotificationChannel(Keys.CHANNEL_ID_OBSERVING_DEVICE_STATE, name, importance).apply {
            description = descriptionText
        }
        // register channel
        createNotificationChannel(channel)
    }


    /* Creates the channel for the alert notification  */
    private fun NotificationManager.createAlertNotificationChannel(context: Context) {
        val name: String = context.getString(R.string.notification_channel_alert_user_name)
        val descriptionText: String = context.getString(R.string.notification_channel_alert_user_description)
        val importance: Int = NotificationManager.IMPORTANCE_HIGH
        val channel = NotificationChannel(Keys.CHANNEL_ID_ALERT_USER, name, importance).apply {
            description = descriptionText
        }
        // register channel
        createNotificationChannel(channel)
    }


    /* Creates the full screen intent */
    private fun getFullScreenIntent(context: Context): PendingIntent {
        val alertActivityIntent: Intent = Intent(context, MainActivity::class.java)
        alertActivityIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_NO_USER_ACTION
        alertActivityIntent.putExtra(Keys.EXTRA_SHOW_ACTIVITY_ON_LOCKSCREEN, true)
        return PendingIntent.getActivity(context, 0, alertActivityIntent, PendingIntent.FLAG_IMMUTABLE)
    }

}