/*
 * DeviceStateObserverService.kt
 * Implements the DeviceStateObserverService service
 * The DeviceStateObserverService observes changes in the device state (for example changes in charging state)
 *
 * This file is part of
 * STAY PUT - Unplug Alert App
 *
 * Copyright (c) 2022-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package org.y20k.stayput

import android.app.KeyguardManager
import android.app.NotificationManager
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.content.pm.ServiceInfo
import android.database.ContentObserver
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.MediaPlayer.OnPreparedListener
import android.net.Uri
import android.os.BatteryManager
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.provider.Settings
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG
import androidx.biometric.BiometricManager.Authenticators.DEVICE_CREDENTIAL
import androidx.core.app.ServiceCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.preference.PreferenceManager
import org.y20k.stayput.helpers.NotificationHelper


class DeviceStateObserverService: Service(), OnPreparedListener {

    /* Define log tag */
    private val TAG: String = DeviceStateObserverService::class.java.simpleName


    /* Main class variables */
    var shouldStayPut: Boolean = false
    var alertIsActive: Boolean = false
    var deviceIsPluggedIn: Boolean = false
    var deviceIsSecured: Boolean = false
    var alarmIsLoudEnough: Boolean = false
    var rePlugIsAllowed: Boolean = true
    private val binder = LocalBinder()
    private var bound: Boolean = false
    private var prepared: Boolean = false
    private var autoAdjustAlarmVolume: Boolean = true
    private var userAlarmVolumeChoice: Int = 0
    private var currentAlarmSound: String = Keys.PREF_ALARM_VALUE_SOUND_1
    private lateinit var notificationManager: NotificationManager
    private lateinit var player: MediaPlayer
    private lateinit var sharedPreferences: SharedPreferences


    /* Overrides onCreate from Service */
    override fun onCreate() {
        super.onCreate()
        // get notification manager
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        // start receiving power connection updates
        val filter = IntentFilter().apply {
            addAction(Intent.ACTION_POWER_CONNECTED)
            addAction(Intent.ACTION_POWER_DISCONNECTED)
        }
        registerReceiver(powerConnectionReceiver, filter)
        // get user preferences and listen for changes
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        rePlugIsAllowed = sharedPreferences.getBoolean(Keys.PREF_ALLOW_REPLUG, true)
        autoAdjustAlarmVolume = sharedPreferences.getBoolean(Keys.PREF_ALLOW_AUTO_ADJUST_ALARM_VOLUME, true)
        currentAlarmSound = sharedPreferences.getString(Keys.PREF_ALARM_SOUND, Keys.PREF_ALARM_VALUE_SOUND_1) ?: Keys.PREF_ALARM_VALUE_SOUND_1
        sharedPreferences.registerOnSharedPreferenceChangeListener(preferencesListener)
        // store the current alarm volume, check if alarm is set loud enough and listen for changes in the system volume
        userAlarmVolumeChoice = getAlarmVolume()
        alarmIsLoudEnough = isAlarmLoudEnough()
        contentResolver.registerContentObserver(Settings.System.CONTENT_URI, true, volumeObserver)
        // get isPluggedIn and start receiving power connection updates
        val batteryManager = getSystemService(Context.BATTERY_SERVICE) as BatteryManager
        deviceIsPluggedIn = batteryManager.isCharging
        registerReceiver(powerConnectionReceiver, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
        // check if device is secured
        deviceIsSecured = isDeviceSecured()
        // setup the player for the alarm sound
        initializePlayer()
    }


    /* Overrides onBind from Service */
    override fun onBind(p0: Intent?): IBinder {
        bound = true
        // start receiving power connection updates
        val filter = IntentFilter().apply {
            addAction(Intent.ACTION_POWER_CONNECTED)
            addAction(Intent.ACTION_POWER_DISCONNECTED)
        }
        registerReceiver(powerConnectionReceiver, filter)
        // todo unregister receiver

        // return reference to this service
        return binder
    }


    /* Overrides onRebind from Service */
    override fun onRebind(intent: Intent?) {
        bound = true
    }


    /* Overrides onUnbind from Service */
    override fun onUnbind(intent: Intent?): Boolean {
        bound = false
        // ensures onRebind is called
        return true
    }


    /* Overrides onDestroy from Service */
    override fun onDestroy() {
        super.onDestroy()
        player.release()
        unregisterReceiver(powerConnectionReceiver)
        contentResolver.unregisterContentObserver(volumeObserver)
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(preferencesListener)
        stopForeground(STOP_FOREGROUND_REMOVE)
    }


    /* Overrides onPrepared from OnPreparedListener */
    override fun onPrepared(player: MediaPlayer) {
        prepared = true
        // start alarm immediately if conditions are met
        if (shouldStayPut && !deviceIsPluggedIn) {
            player.start()
        }
    }


//    /* Toggles the observer notification */
//    fun toggleObserverNotification() {
//        if (shouldStayPut) {
//            // show observer notification
//            val notification = NotificationHelper.displayObserverNotification(this@DeviceStateObserverService)
//            ServiceCompat.startForeground(this, Keys.NOTIFICATION_ID_OBSERVING_DEVICE_STATE, notification, ServiceInfo.FOREGROUND_SERVICE_TYPE_SPECIAL_USE)
//        } else {
//            notificationManager.cancel(Keys.NOTIFICATION_ID_OBSERVING_DEVICE_STATE)
//            ServiceCompat.stopForeground(this, ServiceCompat.STOP_FOREGROUND_REMOVE)
//        }
//    }


    /* Starts observing device state */
    fun startObserving() {
        // store the chosen alarm volume
        userAlarmVolumeChoice = getAlarmVolume()
        // show observer notification
        val notification = NotificationHelper.showObserverNotification(this@DeviceStateObserverService)
        // start this service as a foreground service
        ServiceCompat.startForeground(this, Keys.NOTIFICATION_ID_OBSERVING_DEVICE_STATE, notification, ServiceInfo.FOREGROUND_SERVICE_TYPE_SPECIAL_USE)
    }


    /* Stops observing device state */
    fun stopObserving() {
        // stop alarm first if it is active
        if (alertIsActive) {
            stopAlarm()
        }
        // cancel observer notification
        notificationManager.cancel(Keys.NOTIFICATION_ID_OBSERVING_DEVICE_STATE)
        // remove this service as a foreground service - service will be stopped automatically when all clients unbind
        ServiceCompat.stopForeground(this, ServiceCompat.STOP_FOREGROUND_REMOVE)
    }


    /* Starts the alarm */
    fun startAlarm() {
        alertIsActive = true
        // show alert notification
        NotificationHelper.showAlertNotification(this@DeviceStateObserverService)
        // play alarm sound
        if (prepared && !player.isPlaying) player.start()
    }


    /* Stops the alarm */
    fun stopAlarm() {
        alertIsActive = false
        // stop alert notification
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(Keys.NOTIFICATION_ID_ALERT_USER)
        // pause alarm sound
        if (prepared && player.isPlaying) player.pause()
    }


    /* Initializes MediaPlayer with the alarm sound */
    private fun initializePlayer() {
        player = MediaPlayer().apply {
            setOnPreparedListener(this@DeviceStateObserverService)
            setDataSource(this@DeviceStateObserverService, getAlarmSoundUri())
            setAudioAttributes(
                AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_ALARM)
                    .build()
            )
            isLooping = true
            prepareAsync()
        }
    }


    /* Gets the Uri for the selected alarm sound */
    private fun getAlarmSoundUri(): Uri {
        // get resource ID for selected sound preference
        val resourceId = when (currentAlarmSound) {
            /* Anti-Theft Alarm Sound Credit ("instoreantitheftalarm"):
             * "in store anti theft alarm.wav" by soundslikewillem
             * https://freesound.org/people/soundslikewillem/sounds/377156/
             * licenced under CC BY-NC 4.0
             * https://creativecommons.org/licenses/by-nc/4.0/ */
            Keys.PREF_ALARM_VALUE_SOUND_1 -> R.raw.instoreantitheftalarm

            /* Duck Quack Sound Credit ("duckquack"):
             * "20130403_duck.04.wav" by dobroide
             * https://freesound.org/people/dobroide/sounds/185134/
             * licenced under CC BY-NC 4.0
             * https://creativecommons.org/licenses/by/4.0/ */
            Keys.PREF_ALARM_VALUE_SOUND_2 -> R.raw.duckquack

            // fallback to default sound if preference value not recognized
            else -> R.raw.instoreantitheftalarm
        }
        return Uri.parse("android.resource://$packageName/$resourceId")
    }


    /* Checks if the alarm volume is loud enough */
    private fun isAlarmLoudEnough(): Boolean {
        val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val maxAlarmVolume: Int = audioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM)
        val currentAlarmVolume: Int = getAlarmVolume()
        // return true if the current alarm volume is more than half of the max alarm volume (= 50 percent)
        return currentAlarmVolume > maxAlarmVolume / 2
    }


    /* Checks if device is secured by either PIN, Password, Pattern or Biometric */
    private fun isDeviceSecured(): Boolean {
        val biometricManager = BiometricManager.from(this)
        if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.Q) {
            // Android 11+ (API 30+)
            return biometricManager.canAuthenticate(BIOMETRIC_STRONG or DEVICE_CREDENTIAL) == BiometricManager.BIOMETRIC_SUCCESS
        } else {
            // Android 10 and below
            val keyguardManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
            return keyguardManager.isDeviceSecure
        }
    }


    /* Sends a broadcast that device state has changed */
    private fun sendDeviceStateChangedBroadcast(extra: String, state: Boolean) {
        val deviceStateChangedIntent = Intent()
        deviceStateChangedIntent.action = Keys.ACTION_DEVICE_STATE_CHANGED
        deviceStateChangedIntent.putExtra(extra, state)
        LocalBroadcastManager.getInstance(this).sendBroadcast(deviceStateChangedIntent)
   }


    /* Sets the alarm volume to the stored target value */
    private fun adjustAlarmVolume() {
        val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        if (getAlarmVolume() < userAlarmVolumeChoice) {
            audioManager.setStreamVolume(AudioManager.STREAM_ALARM, userAlarmVolumeChoice, 0) // flags - set to 0 to avoid showing the volume UI
        }
    }


    /* Gets the current alarm volume */
    private fun getAlarmVolume(): Int {
        val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        return audioManager.getStreamVolume(AudioManager.STREAM_ALARM)
    }


    /*
     * Defines a listener for changes in SharedPreferences
     */
    private val preferencesListener = SharedPreferences.OnSharedPreferenceChangeListener { prefs, key ->
        when (key) {
            Keys.PREF_ALLOW_REPLUG -> {
                rePlugIsAllowed = prefs.getBoolean(key, true)
            }
            Keys.PREF_ALLOW_AUTO_ADJUST_ALARM_VOLUME -> {
                autoAdjustAlarmVolume = prefs.getBoolean(key, true)
            }
            Keys.PREF_ALARM_SOUND -> {
                val newSound = prefs.getString(key, Keys.PREF_ALARM_VALUE_SOUND_1) ?: Keys.PREF_ALARM_VALUE_SOUND_1
                if (newSound != currentAlarmSound) {
                    currentAlarmSound = newSound
                    if (!alertIsActive) {
                        player.release()
                        initializePlayer()
                    }
                }
            }
        }
    }
    /*
     * End of declaration
     */


    /*
     * Defines ContentObserver that listens for changes in the system volume
     */
    private val volumeObserver by lazy {
        object : ContentObserver(Handler(Looper.getMainLooper())) {
            override fun onChange(selfChange: Boolean) {
                super.onChange(selfChange)
                alarmIsLoudEnough = isAlarmLoudEnough()
                sendDeviceStateChangedBroadcast(Keys.EXTRA_ALARM_VOLUME_CHANGED, alarmIsLoudEnough)
                if (autoAdjustAlarmVolume && shouldStayPut) adjustAlarmVolume()
            }
        }
    }
    /*
     * End of declaration
     */


    /*
     * Defines a BroadcastReceiver that receives changes in charging state
     */
    private val powerConnectionReceiver by lazy {
        object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val status: Int = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0)
                deviceIsPluggedIn = status != 0 // 0 means device is on battery
                if (deviceIsPluggedIn && shouldStayPut && rePlugIsAllowed) stopAlarm()
                else if (!deviceIsPluggedIn && shouldStayPut) startAlarm()
                sendDeviceStateChangedBroadcast(Keys.EXTRA_DEVICE_PLUGGED_IN, deviceIsPluggedIn)
            }
        }
    }
    /*
     * End of declaration
     */


    /*
     * Inner class: Local Binder that returns this service
     */
    inner class LocalBinder : Binder() {
        val service: DeviceStateObserverService = this@DeviceStateObserverService
    }
    /*
     * End of inner class
     */

}