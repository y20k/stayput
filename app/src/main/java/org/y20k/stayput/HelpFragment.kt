/*
 * HelpFragment.kt
 * Implements the HelpFragment class
 * The HelpFragment displays helpful information about the app
 *
 * This file is part of
 * STAY PUT - Unplug Alert App
 *
 * Copyright (c) 2022-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package org.y20k.stayput

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment


/*
 * HelpFragment class
 */
class HelpFragment: Fragment() {

    /* Define log tag */
    private val TAG: String = HelpFragment::class.java.simpleName


    /* Overrides onCreate from Fragment */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // show action bar
        (activity as AppCompatActivity).supportActionBar?.show()
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.fragment_help_label)
    }


    /* Overrides onCreate from Fragment */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // find views
        val rootView: View = inflater.inflate(R.layout.fragment_help, container, false)
        return rootView
    }

}