/*
 * Keys.kt
 * Implements the keys used throughout the app
 * This object hosts all keys used to control Transistor state
 *
 * This file is part of
 * STAY PUT - Unplug Alert App
 *
 * Copyright (c) 2022-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package org.y20k.stayput


/*
 * Keys object
 */
object Keys {

    // preferences
    const val PREF_ALLOW_REPLUG: String = "ALLOW_REPLUG"
    const val PREF_ALLOW_AUTO_ADJUST_ALARM_VOLUME: String = "ALLOW_AUTO_ADJUST_ALARM_VOLUME"
    const val PREF_ALARM_SOUND: String = "ALARM_SOUND"
    const val PREF_ALARM_VALUE_SOUND_1: String = "ALARM_VALUE_SOUND_1"
    const val PREF_ALARM_VALUE_SOUND_2: String = "ALARM_VALUE_SOUND_2"

    // intent actions
    const val ACTION_DEVICE_STATE_CHANGED: String = "org.y20k.stayput.action.DEVICE_STATE_CHANGED"

    // intent extras
    const val EXTRA_SHOW_ACTIVITY_ON_LOCKSCREEN: String = "SHOW_ACTIVITY_ON_LOCKSCREEN"
    const val EXTRA_DEVICE_PLUGGED_IN: String = "DEVICE_PLUGGED_IN"
    const val EXTRA_ALARM_VOLUME_CHANGED: String = "ALARM_VOLUME_CHANGED"

    // keys
    const val KEY_AUTH_IN_PROGRESS = "auth_in_progress"
    const val KEY_LAST_AUTH_ATTEMPT = "last_auth_attempt"

    // values
    const val AUTH_RETRY_COOLDOWN = 500L // milliseconds

    // notification
    const val CHANNEL_ID_OBSERVING_DEVICE_STATE: String = "channelIdObservingDeviceState"
    const val NOTIFICATION_ID_OBSERVING_DEVICE_STATE: Int = 10
    const val CHANNEL_ID_ALERT_USER: String = "channelIdAlertUser"
    const val NOTIFICATION_ID_ALERT_USER: Int = 11
    const val NOTIFICATION_ID_ALERT_FULLSCREEN: Int = 12

}