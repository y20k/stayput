/*
 * MainFragment.kt
 * Implements the MainFragment class
 * The MainFragment offers to lock / unlock the device
 *
 * This file is part of
 * STAY PUT - Unplug Alert App
 *
 * Copyright (c) 2022-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package org.y20k.stayput

import android.Manifest
import android.app.KeyguardManager
import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG
import androidx.biometric.BiometricManager.Authenticators.DEVICE_CREDENTIAL
import androidx.biometric.BiometricPrompt
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.Group
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textview.MaterialTextView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.concurrent.Executor

/*
 * MainFragment class
 */
class MainFragment: Fragment() {

    /* Define log tag */
    private val TAG: String = MainFragment::class.java.simpleName


    /* Main class variables */
    private var shouldStayPut: Boolean = false
    private var deviceIsPluggedIn: Boolean = false
    private var deviceIsSecured: Boolean = false
    private var alarmIsLoudEnough: Boolean = false
    private var rePlugIsAllowed: Boolean = true
    private var bound: Boolean = false
    private val isMiuiDevice = Build.MANUFACTURER.equals("Xiaomi", ignoreCase = true)
    private lateinit var deviceStateObserverService: DeviceStateObserverService
    private lateinit var deviceStateIcon: ImageView
    private lateinit var setupCheckViews: CardView
    private lateinit var deviceSecuredCheckViews: LinearLayout
    private lateinit var alarmVolumeCheckViews: LinearLayout
    private lateinit var deviceSecuredCheckButton: MaterialButton
    private lateinit var alarmVolumeCheckButton: MaterialButton
    private lateinit var alertTextViews: Group
    private lateinit var alertText: MaterialTextView
    private lateinit var helpAndSettingsViews: Group
    private lateinit var mainButton: MaterialButton
    private lateinit var helpButton: ImageButton
    private lateinit var settingsButton: ImageButton
    private lateinit var executor: Executor
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo


    /* Overrides onCreate from Fragment */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // create prompt for authentication
        createAuthenticationPrompt()
    }


    /* Overrides onCreateView from Fragment */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        // find views
        val rootView: View = inflater.inflate(R.layout.fragment_main, container, false)
        deviceStateIcon = rootView.findViewById(R.id.device_state_icon)
        setupCheckViews = rootView.findViewById(R.id.setup_check_views)
        deviceSecuredCheckViews = rootView.findViewById(R.id.device_secured_check_views)
        alarmVolumeCheckViews = rootView.findViewById(R.id.alarm_volume_check_views)
        alertTextViews = rootView.findViewById(R.id.alert_text_views)
        alertText = rootView.findViewById(R.id.alert_text)
        helpAndSettingsViews = rootView.findViewById(R.id.help_and_settings_views)
        mainButton = rootView.findViewById(R.id.button_main)
        helpButton = rootView.findViewById(R.id.button_help)
        settingsButton = rootView.findViewById(R.id.button_settings)
        deviceSecuredCheckButton = rootView.findViewById(R.id.device_secured_check_change_button)
        alarmVolumeCheckButton = rootView.findViewById(R.id.alarm_volume_check_change_button)

        // get navigation controller
        val navHostFragment = activity?.supportFragmentManager?.findFragmentById(R.id.main_host_container) as NavHostFragment
        val navController = navHostFragment.navController

        // set up main button
        mainButton.setOnClickListener {
            handleMainButtonClick()
        }

        // set up help and settings buttons
        helpButton.setOnClickListener {
            navController.navigate(R.id.help_destination)
        }
        settingsButton.setOnClickListener {
            navController.navigate(R.id.settings_destination)
        }

        // set up device secured and alarm volume check buttons
        deviceSecuredCheckButton.setOnClickListener {
            openSecuritySettings()
        }
        alarmVolumeCheckButton.setOnClickListener {
            openSoundSettings()
        }

        return rootView
    }


    /* Overrides onStart from Fragment */
    override fun onStart() {
        super.onStart()
        // bind to DeviceStateObserverService
        activity?.bindService(Intent(activity, DeviceStateObserverService::class.java), connection, Context.BIND_AUTO_CREATE)
    }


    /* Overrides onResume from Fragment */
    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar?.hide()
    }


    /* Overrides onStop from Fragment */
    override fun onStop() {
        super.onStop()
        // unbind from DeviceStateObserverService and unregister deviceStateChangedReceiver
        if (bound) {
            LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(deviceStateChangedReceiver)
            activity?.unbindService(connection)
            bound = false
        }
    }


    /* Updates the status views */
    fun updateStatusViews() {
        if (bound) {
            // get device state
            deviceIsSecured = deviceStateObserverService.deviceIsSecured
            alarmIsLoudEnough = deviceStateObserverService.alarmIsLoudEnough
            deviceIsPluggedIn = deviceStateObserverService.deviceIsPluggedIn
            rePlugIsAllowed = deviceStateObserverService.rePlugIsAllowed
            shouldStayPut = deviceStateObserverService.shouldStayPut

            // update the alert text
            if (rePlugIsAllowed) alertText.text = getString(R.string.main_alert_text_disarm_or_replug)
            else alertText.text = getString(R.string.main_alert_text_disarm)

            // setup plugged in state icon
            if (deviceIsPluggedIn) {
                deviceStateIcon.setImageResource(R.drawable.ic_device_state_plugged_in_96dp)
                deviceStateIcon.contentDescription = getString(R.string.descr_icon_device_state_plugged_in)
            } else {
                deviceStateIcon.setImageResource(R.drawable.ic_device_state_unplugged_96dp)
                deviceStateIcon.contentDescription = getString(R.string.descr_icon_device_state_unplugged)
            }

            // set button state
            if (!shouldStayPut && (!deviceIsPluggedIn || !deviceIsSecured || !alarmIsLoudEnough)) {
                mainButton.isEnabled = false
            } else {
                mainButton.isEnabled = true
            }

            // set setup check state card
            if ((deviceIsSecured && alarmIsLoudEnough) || shouldStayPut) {
                setupCheckViews.isVisible = false
            } else {
                setupCheckViews.isVisible = true
                deviceSecuredCheckViews.isGone = deviceIsSecured
                alarmVolumeCheckViews.isGone = alarmIsLoudEnough
            }

            // set alert state
            if (!deviceIsPluggedIn && shouldStayPut) {
                alertTextViews.isVisible = true
            } else {
                alertTextViews.isVisible = false
            }

            // set button text
            if (!shouldStayPut) {
                mainButton.text = getString(R.string.button_stay_put)
                helpAndSettingsViews.isVisible = true
            } else {
                mainButton.text = getString(R.string.button_disarm)
                // hide settings once started listening for charging state changes
                helpAndSettingsViews.isVisible = false
            }
        }
    }


    /* Handles tap on main button */
    private fun handleMainButtonClick() {
        if (shouldStayPut) {
            if (isMiuiDevice) {
                // MIUI devices need the lock screen to be dismissed first
                dismissKeyguardThenAuthenticate()
            } else {
                biometricPrompt.authenticate(promptInfo)
            }
        } else {
            checkNotificationPermission()
        }
    }


    /* Toggle the main button state */
    private fun toggleMainButtonState() {
        // toggle shouldStayPut and update user interface
        shouldStayPut = !shouldStayPut
        deviceStateObserverService.shouldStayPut = shouldStayPut
        updateStatusViews()
        // start / stop observing device state
        when (shouldStayPut) {
            // CASE: user tapped "Stay Put"
            true -> {
                activity?.startForegroundService(Intent(activity, DeviceStateObserverService::class.java))
                deviceStateObserverService.startObserving()
            }
            // CASE: user tapped "Disarm"
            false -> {
                deviceStateObserverService.stopObserving() // alarm will be stopped if necessary
            }
        }
    }


    /* Creates the prompt for authentication */
    private fun createAuthenticationPrompt() {
        // create the biometric prompt
        executor = ContextCompat.getMainExecutor(requireContext())
        biometricPrompt = BiometricPrompt(requireActivity(), executor, authenticationCallback)

        // create the prompt info
        val promptInfoBuilder = BiometricPrompt.PromptInfo.Builder().apply {
            setTitle(getString(R.string.unlock_prompt_title))
            setSubtitle(getString(R.string.unlock_prompt_text))
        }
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
            // Android 11+ (API 30+)
            promptInfoBuilder.setAllowedAuthenticators(BIOMETRIC_STRONG or DEVICE_CREDENTIAL)
        } else {
            // Android 10 and below
            promptInfoBuilder.setDeviceCredentialAllowed(true)
        }
        promptInfo = promptInfoBuilder.build()
    }


    /* Dismisses the lock screen and starts authentication - only used on phones using MIUI */
    private fun dismissKeyguardThenAuthenticate() {
        val keyguardManager = requireContext().getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
        if (keyguardManager.isKeyguardSecure && keyguardManager.isKeyguardLocked) {
            keyguardManager.requestDismissKeyguard(
                requireActivity(),
                object : KeyguardManager.KeyguardDismissCallback() {
                    override fun onDismissSucceeded() {
                        // lockscreen has been dismissed, start authentication to disable the alarm
                        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
                            biometricPrompt.authenticate(promptInfo)
                        }
                    }
                    override fun onDismissError() {
                        Log.e(TAG, "Dismissing the lock screen failed")
                    }
                    override fun onDismissCancelled() {
                        Log.d(TAG, "Dismissing the lock screen cancelled")
                    }
                })
        } else {
            // no secure lock screen present, proceed directly to authentication
            biometricPrompt.authenticate(promptInfo)
        }
    }


    /* Opens the device security settings */
    private fun openSecuritySettings() {
        try {
            startActivity(Intent(Settings.ACTION_SECURITY_SETTINGS))
        } catch (e: Exception) {
            try {
                startActivity(Intent(Settings.ACTION_SETTINGS))
            } catch (e: Exception) {
                Toast.makeText(requireContext(), getString(R.string.toast_message_unable_to_open_settings), Toast.LENGTH_SHORT).show()
            }
        }
    }


    /* Opens the device sound settings */
    private fun openSoundSettings() {
        try {
            startActivity(Intent(Settings.ACTION_SOUND_SETTINGS))
        } catch (e: Exception) {
            try {
                startActivity(Intent(Settings.ACTION_SETTINGS))
            } catch (e: Exception) {
                Toast.makeText(requireContext(), getString(R.string.toast_message_unable_to_open_settings), Toast.LENGTH_SHORT).show()
            }
        }
    }


    /* Checks the notification permission and toggles the button if permission is granted */
    private fun checkNotificationPermission() {
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED) {
            // notification permission is granted
            toggleMainButtonState()
        } else if (shouldShowRequestPermissionRationale(Manifest.permission.POST_NOTIFICATIONS)) {
            // explain why notification permission is required
            showNotificationPermissionRationale()
        } else {
            // request permission
            requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
        }
    }


    /* Shows the notification with an explanation why the permission is needed */
    private fun showNotificationPermissionRationale() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.dialog_notification_permission_rationale_title)
            .setMessage(R.string.dialog_notification_permission_rationale_text)
            .setPositiveButton(R.string.dialog_button_ok) { _, _ ->
                requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
            }
            .setNegativeButton(R.string.dialog_button_cancel, null)
            .show()
    }


    /*
     * Defines the ActivityResultLauncher for requesting notification permission
     */
    private val requestPermissionLauncher = registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
        if (isGranted) {
            // notification permission is granted
            toggleMainButtonState()
        } else {
            Toast.makeText(requireContext(), getString(R.string.toast_message_notification_permission_denied), Toast.LENGTH_LONG).show()
        }
    }
    /*
     * End of declaration
     */


    /*
     * Defines AuthenticationCallback that handles results of authentication
     */
    private val authenticationCallback: BiometricPrompt.AuthenticationCallback by lazy {
        object : BiometricPrompt.AuthenticationCallback() {
            override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                super.onAuthenticationError(errorCode, errString)
                Toast.makeText(requireContext(), getString(R.string.toast_message_authentication_error, errString), Toast.LENGTH_SHORT).show()
            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                toggleMainButtonState()
                Toast.makeText(requireContext(), getString(R.string.toast_message_authentication_success), Toast.LENGTH_SHORT).show()
            }

            override fun onAuthenticationFailed() {
                super.onAuthenticationFailed()
                Toast.makeText(requireContext(), getString(R.string.toast_message_authentication_failed), Toast.LENGTH_SHORT).show()
            }
        }
    }
    /*
     * End of declaration
     */


    /*
     * Defines BroadcastReceiver that handles Keys.ACTION_DEVICE_STATE_CHANGED
     */
    private val deviceStateChangedReceiver by lazy {
        object:  BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.hasExtra(Keys.EXTRA_DEVICE_PLUGGED_IN)) {
                    deviceIsPluggedIn = intent.getBooleanExtra(Keys.EXTRA_DEVICE_PLUGGED_IN, false)
                    updateStatusViews()
                } else if (intent.hasExtra(Keys.EXTRA_ALARM_VOLUME_CHANGED)) {
                    alarmIsLoudEnough = intent.getBooleanExtra(Keys.EXTRA_ALARM_VOLUME_CHANGED, false)
                    updateStatusViews()
                }
            }
        }
    }
    /*
     * End of declaration
     */


    /*
     * Defines callbacks for service binding, passed to bindService()
     */
    private val connection by lazy {
        object : ServiceConnection {
            override fun onServiceConnected(className: ComponentName, service: IBinder) {
                bound = true
                // get reference to service
                val binder = service as DeviceStateObserverService.LocalBinder
                deviceStateObserverService = binder.service
                // start listening for device state changes (like changes in charging state)
                LocalBroadcastManager.getInstance(requireContext()).registerReceiver(deviceStateChangedReceiver, IntentFilter(Keys.ACTION_DEVICE_STATE_CHANGED))
                // get plugged in state and update views if necessary
                shouldStayPut = deviceStateObserverService.shouldStayPut
                deviceIsPluggedIn = deviceStateObserverService.deviceIsPluggedIn
                updateStatusViews()
            }
            override fun onServiceDisconnected(arg0: ComponentName) {
                // service has crashed, or was killed by the system
                LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(deviceStateChangedReceiver)
            }
        }
    }
    /*
     * End of declaration
     */

}