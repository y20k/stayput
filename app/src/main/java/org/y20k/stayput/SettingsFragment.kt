/*
 * SettingsFragment.kt
 * Implements the SettingsFragment fragment
 * A SettingsFragment displays the user accessible settings of the app
 *
 * This file is part of
 * STAY PUT - Unplug Alert App
 *
 * Copyright (c) 2022-25 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */


package org.y20k.stayput

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceCategory
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import androidx.preference.SwitchPreferenceCompat
import androidx.preference.contains


/*
 * SettingsFragment class
 */
class SettingsFragment: PreferenceFragmentCompat() {

    /* Define log tag */
    private val TAG: String = SettingsFragment::class.java.simpleName


    /* Main class variables */
    private lateinit var preferenceAllowRePlug: SwitchPreferenceCompat
    private lateinit var preferenceAutoAdjustAlarmVolume: SwitchPreferenceCompat
    private lateinit var preferenceAlarmSound: ListPreference
    private lateinit var preferenceAppVersion: Preference
    private lateinit var preferenceReportIssue: Preference


    /* Overrides onViewCreated from PreferenceFragmentCompat */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // show action bar
        (activity as AppCompatActivity).supportActionBar?.show()
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.fragment_settings_label)
    }


    /* Overrides onCreatePreferences from PreferenceFragmentCompat */
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {

        val screen = preferenceManager.createPreferenceScreen(preferenceManager.context)

        // set up "App Theme" preference
        preferenceAllowRePlug = SwitchPreferenceCompat (requireContext())
        preferenceAllowRePlug.title = getString(R.string.pref_allow_replug_title)
        preferenceAllowRePlug.setIcon(R.drawable.ic_cable_24dp)
        preferenceAllowRePlug.key = Keys.PREF_ALLOW_REPLUG
        preferenceAllowRePlug.summaryOn = getString(R.string.pref_allow_replug_summary_on)
        preferenceAllowRePlug.summaryOff = getString(R.string.pref_allow_replug_summary_off)
        preferenceAllowRePlug.setDefaultValue(true)

        // set up "Auto-adjust Volume" preference
        preferenceAutoAdjustAlarmVolume = SwitchPreferenceCompat(requireContext())
        preferenceAutoAdjustAlarmVolume.title = getString(R.string.pref_auto_adjust_alarm_volume_title)
        preferenceAutoAdjustAlarmVolume.setIcon(R.drawable.ic_volume_up_24dp)
        preferenceAutoAdjustAlarmVolume.key = Keys.PREF_ALLOW_AUTO_ADJUST_ALARM_VOLUME
        preferenceAutoAdjustAlarmVolume.summaryOn = getString(R.string.pref_auto_adjust_alarm_volume_summary_on)
        preferenceAutoAdjustAlarmVolume.summaryOff = getString(R.string.pref_auto_adjust_alarm_volume_summary_off)
        preferenceAutoAdjustAlarmVolume.setDefaultValue(true)


        // set up "Alarm Sound" preference
        preferenceAlarmSound = ListPreference(requireContext())
        preferenceAlarmSound.title = getString(R.string.pref_alarm_sound_title)
        preferenceAlarmSound.setIcon(R.drawable.ic_music_note_24dp)
        preferenceAlarmSound.key = Keys.PREF_ALARM_SOUND
        preferenceAlarmSound.entries = arrayOf(getString(R.string.pref_alarm_sound_entry_1), getString(R.string.pref_alarm_sound_entry_2))
        preferenceAlarmSound.entryValues = arrayOf(Keys.PREF_ALARM_VALUE_SOUND_1, Keys.PREF_ALARM_VALUE_SOUND_2)
        preferenceAlarmSound.setDefaultValue(Keys.PREF_ALARM_VALUE_SOUND_1)
        preferenceAlarmSound.summary = "${getString(R.string.pref_alarm_sound_summary)} ${getAlarmSoundName()}"
        preferenceAlarmSound.setOnPreferenceChangeListener { preference, newValue ->
            if (preference is ListPreference) {
                val index: Int = preference.entryValues.indexOf(newValue)
                preferenceAlarmSound.summary = "${getString(R.string.pref_alarm_sound_summary)} ${preference.entries[index]}"
                return@setOnPreferenceChangeListener true
            } else {
                return@setOnPreferenceChangeListener false
            }
        }


        // set up "App Version" preference
        preferenceAppVersion = Preference(requireContext())
        preferenceAppVersion.title = getString(R.string.pref_app_version_title)
        preferenceAppVersion.setIcon(R.drawable.ic_info_24dp)
        preferenceAppVersion.summary = "${getString(R.string.pref_app_version_summary)} ${BuildConfig.VERSION_NAME} (${getString(R.string.app_version_name)})"
        preferenceAppVersion.setOnPreferenceClickListener {
            // copy to clipboard
            val clip: ClipData = ClipData.newPlainText("simple text", preferenceAppVersion.summary)
            val cm: ClipboardManager = preferenceManager.context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            cm.setPrimaryClip(clip)
            Toast.makeText(requireContext(), R.string.toast_message_copied_to_clipboard, Toast.LENGTH_LONG).show()
            return@setOnPreferenceClickListener true
        }

        // set up "Report Issue" preference
        preferenceReportIssue = Preference(requireContext())
        preferenceReportIssue.title = getString(R.string.pref_report_issue_title)
        preferenceReportIssue.setIcon(R.drawable.ic_bug_report_24dp)
        preferenceReportIssue.summary = getString(R.string.pref_report_issue_summary)
        preferenceReportIssue.setOnPreferenceClickListener {
            // open web browser
            val intent = Intent().apply {
                action = Intent.ACTION_VIEW
                data = "https://codeberg.org/y20k/stayput/issues".toUri()
            }
            startActivity(intent)
            return@setOnPreferenceClickListener true
        }


        // set preference categories
        val preferenceCategoryGeneral: PreferenceCategory = PreferenceCategory(requireContext())
        preferenceCategoryGeneral.title = getString(R.string.pref_general_title)
        preferenceCategoryGeneral.contains(preferenceAllowRePlug)
        preferenceCategoryGeneral.contains(preferenceAutoAdjustAlarmVolume)
        preferenceCategoryGeneral.contains(preferenceAlarmSound)

        val preferenceCategoryAbout: PreferenceCategory = PreferenceCategory(requireContext())
        preferenceCategoryAbout.title = getString(R.string.pref_about_title)
        preferenceCategoryAbout.contains(preferenceAppVersion)
        preferenceCategoryAbout.contains(preferenceReportIssue)


        // setup preference screen
        screen.addPreference(preferenceCategoryGeneral)
        screen.addPreference(preferenceAllowRePlug)
        screen.addPreference(preferenceAutoAdjustAlarmVolume)
        screen.addPreference(preferenceAlarmSound)

        screen.addPreference(preferenceCategoryAbout)
        screen.addPreference(preferenceAppVersion)
        screen.addPreference(preferenceReportIssue)

        preferenceScreen = screen
    }


    /* Gets the name of the alarm sound that is currently in use */
    private fun getAlarmSoundName(): String {
        val sharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext())
        val currentAlarmSound: String = sharedPreferences.getString(Keys.PREF_ALARM_SOUND, Keys.PREF_ALARM_VALUE_SOUND_1) ?: Keys.PREF_ALARM_VALUE_SOUND_1
        return when (currentAlarmSound) {
            Keys.PREF_ALARM_VALUE_SOUND_1 -> getString(R.string.pref_alarm_sound_entry_1)
            Keys.PREF_ALARM_VALUE_SOUND_2 -> getString(R.string.pref_alarm_sound_entry_2)
            else -> {
                getString(R.string.pref_alarm_sound_entry_1)
            }
        }
    }

}
