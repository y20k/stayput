AUTHORS
=======

### Development
Stay Put is designed, developed and maintained by: [y20k](https://codeberg.org/y20k)

### Want to help?
Please check out the notes in [CONTRIBUTE.md](https://codeberg.org/y20k/stayput/src/branch/main/CONTRIBUTE.md) first.