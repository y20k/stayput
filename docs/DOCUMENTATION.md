# Stay Put - Development Documentation

## Core Purpose
- Anti-theft alarm app for phones charging in public spaces
- Sounds alarm when device is unplugged
- Targets travelers/campers who need to charge in shared spaces

## Key Features
1. Charging state monitoring
2. Alarm triggering on unplug
3. Two ways to stop alarm:
   - Authentication (device unlock methods)
   - Reconnecting to power (optional feature)
4. Foreground service for background operation
5. Biometric/PIN authentication
6. Full-screen and normal notifications

## Technical Implementation

### Service
- `DeviceStateObserverService`: Core service monitoring charging state
- Runs as foreground service with persistent notification
- Uses `powerConnectionReceiver` to monitor charging state
- Handles alarm sound playback
- Important: powerConnectionReceiver must be registered in onCreate() of service, not in onBind()

### Authentication
- Uses BiometricPrompt API
- Supports fingerprint, face recognition, and device PIN
- Required for disarming alarm through UI or notification
- Dependencies:
  ```gradle
  implementation "androidx.biometric:biometric:1.1.0"
  ```
- Version-specific implementation:
  - Android 11+ (API 30+): Uses `setAllowedAuthenticators(BIOMETRIC_STRONG or DEVICE_CREDENTIAL)`
  - Android 10 and below: Uses `setDeviceCredentialAllowed(true)` for compatibility

- MIUI-specific handling:
  - MIUI devices require special treatment due to their lock screen behavior
  - Uses KeyguardManager.requestDismissKeyguard() before authentication
  - Only called when both isKeyguardSecure and isKeyguardLocked are true
  - Prevents authentication issues specific to Xiaomi's MIUI implementation

- Note: While emulators might accept newer authentication methods on older Android versions, real devices require version-specific handling
- Reference: CommonsWare's "BiometricPrompt and Weak Biometrics" guide explains the version-specific requirements
  (https://commonsware.com/R/pages/chap-security-002.html)
- Minimum SDK: 23 (Android 6.0)
- Target SDK: 34 (Android 14)

### Notifications
- Two types: observer (foreground service) and alert
- Alert notification includes:
  - Full-screen intent for lock screen
  - Disarm action button
  - Uses `Keys.NOTIFICATION_ID_ALERT_USER`
- Known Issue: Notification cancellation requires careful handling due to full-screen intent
- Solution: Use single notification ID and ensure proper cancellation timing

### Settings
- Allow reconnect option (can disable alarm by plugging back in)
- Stored in SharedPreferences
- Preference changes monitored via SharedPreferences.OnSharedPreferenceChangeListener

### State Management
- `shouldStayPut`: Controls if service is actively monitoring
- `alertIsActive`: Tracks if alarm is currently sounding
- States are managed in DeviceStateObserverService
- UI reflects states through LocalBroadcastManager
- State changes trigger appropriate UI updates and notifications

### Alarm Sound
- Located in: res/raw/instoreantitheftalarm.wav
- Credit: "in store anti theft alarm.wav" by soundslikewillem
- License: CC BY-NC 4.0
- Implementation: Uses MediaPlayer with USAGE_ALARM and looping enabled

### Critical Test Scenarios
1. Battery Optimization:
   - Service must remain active despite battery optimization
   - Test on various Android versions
   - Verify notification persistence

2. Screen States:
   - Test alarm triggering when screen is off
   - Verify full-screen notification appears on lock screen
   - Check biometric prompt appears properly
   - Handle fragment recreation on lock screen (important for authentication)

3. Edge Cases:
   - Rapid plug/unplug events
   - Service recreation by system
   - Incoming calls during alarm
   - Multiple notification interactions
   - Multiple authentication attempts during fragment recreation

### UI/UX Implementation
- Material Design 3 components used throughout
- Single-activity architecture with fragments
- Help screen designed to fit on one screen
- Setup card shows system requirements
- Notification actions mirror UI actions for consistency
- Title case used for all headings

### Specific Challenges Solved
1. Notification Handling:
   - Issue: Full-screen intent created separate notification
   - Solution: Single notification with both regular and full-screen components

2. Power Connection Monitoring:
   - Issue: Receiver not working in foreground service
   - Solution: Register receiver in service onCreate instead of onBind

3. Service Reliability:
   - Issue: Service killed by system
   - Solution: Proper foreground service implementation with persistent notification

4. Authentication Flow:
   - Issue: Multiple authentication prompts on lock screen
   - Solution: Track authentication state during fragment recreation
   - Note: Fragment shown above lock screen may be recreated multiple times

## File Structure
Key files and their purposes:
- `